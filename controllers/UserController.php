<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 03.10.2018
 * Time: 12:40
 */

namespace app\controllers;

use Yii;
use app\models\LoginForm;
use app\yii2\rest\ActiveController;
use yii\web\ForbiddenHttpException;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\User';

    public function behaviors()
    {
        $behaviors =  parent::behaviors();

        $behaviors['authenticator']['only'] = ['index', 'create', 'update', 'delete'];
        $behaviors['access']['only'] = ['index', 'create', 'update', 'delete'];

        return $behaviors;
    }

    public function actions(){
        $actions = parent::actions();
        unset($actions['view']);

        return $actions;
    }

    public function actionView(){
        $loginForm = new LoginForm();
        $loginForm->load(Yii::$app->request->get(), '');
        $token = $loginForm->login();
        if(!empty($token)){
            return [
                'token' => $token,
            ];
        }

        return $loginForm;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        $user = Yii::$app->user->identity;
        if(!$user->admin){
            throw new ForbiddenHttpException('Forbidden.');
        }
    }

}