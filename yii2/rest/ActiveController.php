<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 03.10.2018
 * Time: 20:57
 */

namespace app\yii2\rest;

use app\models\UserIdentity;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;

class ActiveController extends \yii\rest\ActiveController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator']['only'] = ['create', 'update', 'delete'];

        $behaviors['authenticator']['authMethods'] = [
            ['class' => HttpBasicAuth::className(),
                'auth' => [$this, 'auth'],
            ],
            HttpBearerAuth::className(),
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => ['create', 'update', 'delete'],
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        return $behaviors;
    }

    public function auth($username, $password)
    {
        $user = UserIdentity::findOne(['username' => $username]);
        return $user->validatePassword($password) ? $user : null;
    }
}