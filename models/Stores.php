<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stores".
 *
 * @property int $id
 * @property string $name
 * @property string $adress
 * @property string $phone_number
 */
class Stores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stores';
    }

    public function getCars()
    {
        return $this->hasMany(Cars::className(), ['store_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'adress', 'phone_number'], 'required'],
            [['name'], 'string', 'max' => 20],
            [['adress'], 'string', 'max' => 120],
            [['phone_number'], 'string', 'max' => 16],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'adress' => 'Adress',
            'phone_number' => 'Phone Number',
        ];
    }

    public function extraFields()
    {
        return ['cars'];
    }
}
