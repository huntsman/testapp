<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "cars".
 *
 * @property int $id
 * @property int $store_id
 * @property string $name
 * @property string $reg_number
 */
class Cars extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cars';
    }

    public function getStore()
    {
        return $this->hasOne(Stores::className(), ['id' => 'store_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['store_id', 'name', 'reg_number'], 'required'],
            [['store_id'], 'integer'],
            [['name'], 'string', 'max' => 60],
            [['reg_number'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_id' => 'Store ID',
            'name' => 'Name',
            'reg_number' => 'Reg Number',
        ];
    }

    public function extraFields()
    {
        return ['store'];
    }
}
